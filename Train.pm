package Train;

use Moose;
use Network;

use Data::Dumper;

has 'network' => (is => 'rw', required => 1);
has 'trainingSet' => (is => 'rw', required => 1, trigger => \&validateTrainingSet);
has 'learningRate' => (is => 'rw', default => 1);
has 'iterations' => (is => 'rw', required => 0, default => 1000);
has 'printError' => (is => 'rw', required => 0, default => 1);
has 'validateSet' => (is => 'ro', required => 1);

sub validateTrainingSet {
    my ($self, $trainingSet) = @_;

    if(!defined($trainingSet)) {
        print "Setting alternative trainingSet\n";
        $trainingSet = $self->trainingSet;
    }

    return unless $self->validateSet();

    my $networkLayers = $self->network->layers;
    my $sumOfNeuronsInInput = scalar(@{$networkLayers->[0]->neurons});
    my $sumOfNeuronsOutputLayer = scalar(@{$networkLayers->[scalar(@$networkLayers)-1]->neurons});

    #validate each trainingSet
    # foreach my $trainingPair (@$trainingSet) {
    while ($trainingSet->{'hasNext'}->()) {
        my $trainingPair = $trainingSet->{'getTrainingPair'}->();

        my $trainingInputLength = scalar(@{$trainingPair->[0]});
        my $trainingOutputLength = scalar(@{$trainingPair->[1]});
        if($trainingInputLength != $sumOfNeuronsInInput-1) {
            die "Error at Train.pm 'validateTrainingSet' function: trainingSet inputLayer does not match network inputLayer\n";
        }
        if($trainingOutputLength != $sumOfNeuronsOutputLayer-1) {
            print "tol: $trainingOutputLength ;; sonol: $sumOfNeuronsOutputLayer\n";
            die "Error at Train.pm 'validateTrainingSet' function: outputLayer does not match network outputLayer\n";
        }
    }
    return;
}

sub train {
    my ($self) = @_;
    my $trainingSet = $self->trainingSet;
    my $network = $self->network;
	my $layers = $network->layers;
	my $lastLayer = $layers->[scalar(@$layers)-1];
    my $printError = $self->printError();
    my $iterations = $self->iterations;
    # for(0..$self->iterations) {
        # foreach my $trainingPair (@$trainingSet) {
        my $iter = 1;
        while ($trainingSet->{'hasNext'}->()) {
            return if $iter > $iterations;
            my $trainingPair = $trainingSet->{'getTrainingPair'}->();
            $network->setLayer(0, $trainingPair->[0]);
            $network->play;
#             print "After Playing: ". Dumper($network->layers)."\n";
            $self->backProp($trainingPair);

			my $error = $lastLayer->layerError;
            if($printError) {
                if($iter % 500 == 0) {
                    print "$iter || Error: $error\n";
                }
            }
            $iter++;
        }
    # }
}

#backPropogation function
sub backProp {
    my ($self, $trainingPair) = @_;
    $self->calcOutputError($trainingPair->[1]);
	my $layers = $self->network->layers;

	#updating network errors
	# NOTE: ignoring last layer in this loop
	for(my $i=scalar(@$layers)-2; $i>=0; $i--) {
		my $layerNeurons = $layers->[$i]->neurons;
		my $nextLayerNeurons = $layers->[$i+1]->neurons;
		for(my $y=0; $y<scalar(@$layerNeurons); $y++) {
			my $nrn = $layerNeurons->[$y];
			$self->derErrOnOut($nrn, $nextLayerNeurons);
			$self->updateWeight($nrn, $nextLayerNeurons);

		}
	}
}

sub updateWeight {
	my ($self, $nrn, $nextLayerNeurons) = @_;
	my $nrnVal = $nrn->[1];
	my $nrnConn = $nrn->[0];
	my $learningRate = $self->learningRate;
	for(my $i=0; $i<scalar(@$nrnConn); $i++) {
		my $weight = $nrnConn->[$i];
		my $nextNrn = $nextLayerNeurons->[$i];
		my $nextNrnVal = $nextNrn->[1];

		my $nextNrnError = $nextNrn->[2];
		my $derOutOnNet = $nextNrnVal*(1-$nextNrnVal);

		my $weightErr = $nrnVal * $derOutOnNet * $nextNrnError;
		$nrnConn->[$i] = $weight - ($learningRate*$weightErr);
		my $newWeight = $nrnConn->[$i];
	}
}

#calculating the network output error, recieves target output
#for each neuron in last layer sets in idx 2 its own error(target-output) and for last layer setting layerError(0.5*(target-output)**2)
sub calcOutputError {
    my ($self, $targetOutput) = @_;
    my $layers = $self->network->layers;
    my $lastLayer = $layers->[scalar(@$layers)-1];
    my $layerTotalError = 0;
    for(my $i=0; $i<scalar(@{$targetOutput}); $i++) {
        my $nrn = $lastLayer->neurons->[$i];
        my $nrnVal = $nrn->[1];
        my $target = $targetOutput->[$i];
        my $error = $target-$nrnVal;
        $nrn->[2] = -$error;
        $layerTotalError += $error**2;
    }
    $layerTotalError *= 0.5;
    $lastLayer->layerError($layerTotalError);
    return $layerTotalError;
}

sub derErrOnOut {
	my ($self, $nrn, $nextLayerNeurons) = @_;
	my $nrnConn = $nrn->[0];
	my $nrnErrCounter = 0;
	for(my $i=0; $i<scalar(@$nrnConn); $i++) {
		my $nextNrn = $nextLayerNeurons->[$i];

		my $nextNrnVal = $nextNrn->[1];
		my $nextNrnDeltaError = $nextNrn->[2];
		my $derOutOnNet = $nextNrnVal*(1-$nextNrnVal);
		my $weight = $nrn->[0]->[$i];

		my $derErrOnNet = $nextNrnDeltaError * $derOutOnNet * $weight;
		$nrnErrCounter += $derErrOnNet;
	}
	$nrn->[2] = $nrnErrCounter;
	return $nrnErrCounter;
}




1;
