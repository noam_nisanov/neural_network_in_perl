package Layer;


use Moose;

use Data::Dumper;

my $idCounter = 0;

# neuron structure is array with 3 entries:
# 0 - weights for next layer neurons
# 1 - neuron sigmoid value
# 2 - neuron deltaError

###########################################################
#---------------------IMPORTANT---------------------------#
#---------------------------------------------------------#
#NOTICE THAT EACH LAYER LAST NEURON STANDS FOR BIAS NEURON#
#---------------------------------------------------------#
#---------------------IMPORTANT---------------------------#
###########################################################

has 'neurons' => (is => 'rw', default => sub {[]});
has 'nextLayer' => (is => 'rw');
has 'id' => (is => 'ro', default => sub {$idCounter++});
has 'layerError' => (is => 'rw', default => 0);

sub build {
    my ($self, $numOfNeurons) = @_;
    my $neurons = $self->neurons;
    for (my $i=0; $i<$numOfNeurons; $i++) {
        push(@$neurons, [[], 0, 0]);
    }
    #this is the bias neuron
    push(@$neurons, [[], 1, 0]);
}

sub connect {
    my ($self, $connectTo) = @_;
    $self->nextLayer($connectTo);
    
    my $myNeurons = $self->neurons;
    my $toNeurons = $connectTo->neurons;
    #print "Connecting!, to nrns: ".scalar(@$toNeurons)."\n";
    foreach my $myNrn (@$myNeurons) {
        for (my $i=0; $i<scalar(@$toNeurons)-1; $i++) {
            #toNeurons last element is bias neuron which is not to be connected to
            my $weight = rand(1);
            push(@{$myNrn->[0]}, $weight);
        }
    }
}

sub play {
    my $self = shift;
    my $nextLayer = $self->nextLayer;
    my $nextLayerGainedWeights = [];
    my $neurons = $self->neurons;
    my $nextLayerNeurons = $nextLayer->neurons;
    
    #calculating nextLayer weightsSum
    for (my $i=0; $i<scalar(@$neurons); $i++) {
        my $myNrn = $neurons->[$i];
        my $myNeuronValue = $myNrn->[1];
#         print Dumper($myNrn)."\n";
        for (my $y=0; $y<scalar(@{$myNrn->[0]}); $y++) {
            my $tmp = ($myNrn->[0]->[$y]);
#             print "y: $y ;; ADDING TO $y :: wight: $tmp ;; nrnVal: $myNeuronValue;;\n";
            $nextLayerGainedWeights->[$y] += ($myNeuronValue*$myNrn->[0]->[$y]);
        }
    }
    
    for(my $i=0; $i<scalar(@$nextLayerGainedWeights); $i++) {
        my $nextLayerNrn = $nextLayerNeurons->[$i];
        #print "i: $i, bias: $bias\n";
        
        #updating the next layer neurons value
        $nextLayerNrn->[1] = sigmoid($nextLayerGainedWeights->[$i]);
    }
    
    #print "PLAYED: ".Dumper($nextLayerGainedWeights)."\n";
}

sub sigmoid {
    my ($weightsGained) = @_;
    return 1/(1+2.71828**(-($weightsGained)));
}

1;