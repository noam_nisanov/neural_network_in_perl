package Network;

use Moose;
use Layer;

use Data::Dumper;



has 'layers' => (is => 'rw', default => sub {[]});


#function creates the network
sub build {
    my ($self, $layer_structure) = @_;
    my $layers = $self->layers;
    
    my $tmpLayers = [];
    #creating the layers
    foreach my $layer_neuron_sum (@$layer_structure) {
        my $layer = Layer->new();
        $layer->build($layer_neuron_sum);
        push(@$tmpLayers, $layer);
    }


    #connecting the layers
    #print "XXX: ".scalar(@$tmpLayers)."\n";
    for (my $i=0; $i<(scalar(@$tmpLayers)-1); $i++) {
        my $myLayer = $tmpLayers->[$i];
        my $toLayer = $tmpLayers->[($i+1)];
        $myLayer->connect($toLayer);
    }
    $self->layers($tmpLayers);
    return;
}

sub play {
    #run network by default from layer number 1
    #if want to start from different layer, function takes as argument starting layer
    my ($self, $startingLayer) = @_;
    
    if(!defined($startingLayer)) {
        $startingLayer = 0;
    }
    my $layers = $self->layers;    
    #running network algorithm itself, notice it does not run on last layer
    for(my $i=$startingLayer; $i<scalar(@$layers)-1; $i++) {
        $layers->[$i]->play;
    }
}

sub setLayer {
    my ($self, $layerIdx, $layerInfo) = @_;
    my $layer = $self->layers->[$layerIdx]->neurons;
    if(scalar(@$layer)-1 != (scalar(@$layerInfo))) {
        my $layerLength = scalar(@$layer);
        my $layerInfo = scalar(@$layerInfo)-1;
        die "Incorrect data at: Network.pm 'setLayer' function, layerInfo length does not match the layerIdx length\nlayerLength: $layerLength, layerInfo: $layerInfo\n";
    }
    for(my $i=0; $i<scalar(@$layerInfo); $i++) {
        $layer->[$i]->[1] = $layerInfo->[$i];
    }
    return;
}


1;