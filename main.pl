

use strict;
use warnings;

use Network;
use Train;

use Data::Dumper;

$|++;

#use Exx;

# a demonstraition of the neural network module, generates simple network which learns
# to identify which number is bigger between 2 numbers via a large dataset to learn from
# after done training we can test it via the command line
sub main {
    my $layer_structure = [2,3,2];
    my $network = Network->new();
    $network->build($layer_structure);

    #####################################################################
    ### NOTE that we moved to iterator, this is invalid training set! ###
    #####################################################################

    # the training set is an iterator which generates new training set each call
    # thus allows us more flexibility, 50 means numbers will be generated between 0-50
    my $trainingSet = getTrainingSetDemo(50);

    # training example:
    #                     [[7   ,1   ],[1,0]],
    #                     [[6   ,3   ],[1,0]],
    #                     [[5   ,9   ],[0,1]],
    #                     [[3   ,2   ],[1,0]],
    #                     [[5   ,2   ],[1,0]],
    #                     [[0   ,7   ],[0,1]],
    #                     [[3   ,6   ],[0,1]],
    #                     [[2   ,4   ],[0,1]],
    #                     [[8   ,13  ],[0,1]],
    #                     [[523 ,293 ],[1,0]],
    #                     [[8   ,1000],[0,1]],
    #                     [[1000,1   ],[1,0]],
    #                     [[8   ,7   ],[1,0]],
    #                     [[2000,2001],[0,1]],
    #                     [[3001,3000],[1,0]],
    #                   ];

    #netExample($network);

    #$network->setLayer(0, [0.05,0.10]);
    #$network->play;

    # print "Example: ".Dumper($trainingSet->{'getTrainingPair'}->());
    # print "Example: ".Dumper($trainingSet->{'getTrainingPair'}->());
    # print "Example: ".Dumper($trainingSet->{'getTrainingPair'}->());
    # return;

    my $train = Train->new(network => $network,
                           trainingSet => $trainingSet,
                           learningRate => 0.08,
                           iterations => 100000,
                           printError => 1,
                           validateSet => 0,
                           );
    print "Start training!\n";
    $train->train;
    # print Dumper($network)."\n";
     test($network, 2);
}

sub getTrainingSetDemo {
    my $maxRange = shift;
    my $trainSetGen = sub {
        my $num1 = int(rand($maxRange));
        my $num2 = int(rand($maxRange));
        $num1++ if($num1 == $num2);
        my $inputs = [$num1, $num2];
        my $expectedOutput = [0,1];
        if($num1 > $num2) {
            $expectedOutput = [1,0];
        }
        return [$inputs, $expectedOutput];
    };
    my $trainDemoObj = {
        'hasNext' => sub { 1 },
        'getTrainingPair' => $trainSetGen
    };
    return  $trainDemoObj;
}

sub test {
    my ($network, $inputLen) = @_;
    print "Testing:\n";
    while(1) {
        print "\n";
        print "enter first number\n";
        my $x=<>;
        my $in = [];
        push(@$in, $x);
        print "enter second number\n";
        $x=<>;
        push(@$in, $x);
        $network->setLayer(0, $in);
        $network->play;
        my $layers = $network->layers;
        my $out = $layers->[scalar(@$layers)-1]->neurons;
        my $flag = 'smaller';
        if($out->[0]->[1] > $out->[1]->[1]) { $flag = 'bigger'; }
        print "output: ".$out->[0]->[1].' '.$out->[1]->[1]." $flag\n";
    }
}

sub netExample {
    my $net = shift;
    my $layers = $net->layers;
    my $weights = [
                    [
                        [0.15,0.25],
                        [0.20,0.30],
                    ],
                    [
                        [0.40,0.50],
                        [0.45,0.55],
                    ]
                  ];
    my $bias = [0.35,0.60];
    for(my $i=0; $i<2; $i++) {
        my $layer = $layers->[$i]->neurons;
        my $w = $weights->[$i];
        for (my $y=0;$y<2;$y++) {
            my $nrn = $layer->[$y];
            $nrn->[0] = $w->[$y];
        }
        my $nrn = $layer->[2];
        foreach my $conn (@{$nrn->[0]}) {
            $conn = $bias->[$i];
        }
    }

}


main();
